@extends('main')

@section('content')
    @include('pages.home.01_home-era')
    @include('pages.home.02_home-form')
    @include('pages.home.03_home-recently')
    @include('pages.home.04_home-statistics')
    @include('pages.home.05_home-reviews')
    @include('pages.home.06_home-video')
    @include('pages.home.07_home-contacts')
@endsection