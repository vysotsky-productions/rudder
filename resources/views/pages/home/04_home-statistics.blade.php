<section class="statistics">
    <div class="statistics_container">
        <ul class="statistics_list">
            <li class="statistics_item">
                <h3 class="statistics_item-title">
                    826
                </h3>
                <p class="statistics_item-subtitle">
                    Сервисев подключено
                </p>
            </li>
            <li class="statistics_item">
                <h3 class="statistics_item-title">
                    826
                </h3>
                <p class="statistics_item-subtitle">
                    Отремонтировано машин
                </p>
            </li>
            <li class="statistics_item">
                <h3 class="statistics_item-title">
                    8276
                </h3>
                <p class="statistics_item-subtitle">
                    Оформлено полисов
                </p>
            </li>
            <li class="statistics_item">
                <h3 class="statistics_item-title">
                    8276
                </h3>
                <p class="statistics_item-subtitle">
                    Человек доверяют нам
                </p>
            </li>
        </ul>
    </div>
</section>