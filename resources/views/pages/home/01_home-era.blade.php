<section class="era">
    <div class="era_container">
        <div class="era_box">
            <div class="era_box-content">
                <h1 class="era_title">
                    Новая<br>
                    автомобильная эра
                </h1>
                <p class="era_subtitle">
                    Подзаголовок раскрываем смысл заголовка и что человек получит
                    на нашемсервисе. Подзаголовок может быть длинным
                </p>
                <a class="era_button" href="">
                    вход/регистрация
                </a>

                @include('components.auth.auth')

                <div class="reg">
                    <div class="reg_links">
                        <a href="#" class="reg_link mod_1 mod_active">Для автовладельцев</a>
                        <a href="#" class="reg_link mod_2">Для автосервисов</a>
                    </div>


                    <div class="reg_tab mod_1 mod_active">
                        @include('components.auth.reg-drivers')
                    </div>

                    <div class="reg_tab mod_2">
                        @include('components.auth.reg-services')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>