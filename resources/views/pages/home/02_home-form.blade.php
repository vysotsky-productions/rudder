@php
    $brands= [
        1 => [
            'brand' => 'Acura',
            'models' =>
            [
                1 => 'CL',
                2 => 'EL'
            ]
        ],
        2 => [
            'brand' => 'Alfa Romeo',
            'models' =>
            [
                1 => '33',
                2 => '57'
            ]
        ],
    ];
@endphp

<script>
    var brands = <?= json_encode($brands) . ';' ?>
    console.log(brands);
</script>

<section class="form">
    <div class="form_container">
        <div class="form_box mod_left">
            <h2 class="form_title">
                Мы заставляем сервисы бороться
                за ваш автомобиль
            </h2>
            <p class="form_subtitle">
                Вам не нужно искать автосервис - мы сделали это за вас.
                Заполните простую форму иполучите предложение от проверенных сервисов
            </p>
        </div>
        <div class="form_box mod_right">
            <h4 class="form_question">Как мы можем вам помочь?</h4>

            <!-- Step 1 -->
            <div class="form_tab mod_step-1" style="display: block;">
                <textarea class="form_area" name="question"
                          placeholder="Опишите, что случилось с автомобилем?"></textarea>
                <div class="form_fields">
                    <div class="form_field">
                        <input id="file" type="file" name="file">
                        <label class="form_field-label" for="file">+ Добавить фото</label>
                    </div>
                    <button class="form_button mod_to-step-2">
                        Далее
                    </button>
                </div>
            </div>

            <!-- Step 2 -->
            <div class="form_tab mod_step-2" style="display: none;">
                <div class="form_fields">
                    <div class="form_field">
                        <select class="mod_select2" id="brands" name="brand">
                            <option class="default" value="0" disabled="" selected="selected">Марка</option>
                            @foreach($brands as $brand_key => $brand)
                                <option value="{{ $brand_key }}">{{ $brand['brand'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form_field mod_models">
                        <select class="mod_select2" name="models" id="models">
                            <option class="default" value="0" disabled="" selected="selected">Модель</option>
                        </select>
                    </div>
                </div>

                <div class="form_fields">
                    <div class="form_field">
                        <input class="form_text" id="text" type="year" name="year" placeholder="Год">
                    </div>

                    <button class="form_button  mod_to-step-3">
                        Далее
                    </button>
                </div>
            </div>


            <!-- Step 3 -->
            <div class="form_tab mod_step-3" style="display: none;">

                <div class="form_fields">
                    <div class="form_field mod_wide">
                        <input class="form_text" id="email" type="email" name="email" placeholder="E-mail">
                    </div>
                </div>

                <div class="form_fields">
                    <div class="form_field">
                        <input class="form_text" id="phone" type="phone" name="phone" placeholder="Телефон">
                    </div>

                    <button class="form_button mod_request">
                        Далее
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


<script>
    $(document).ready(function () {
        $('.mod_select2').select2();

        $('.form_button.mod_to-step-2').click(function () {
            $('.form_tab.mod_step-1').hide();
            $('.form_tab.mod_step-2').show();
        });

        $('.form_button.mod_to-step-3').click(function () {
            $('.form_tab.mod_step-2').hide();
            $('.form_tab.mod_step-3').show();
        });



        $('#brands').on('select2:select', function (e) {
            $("#models option:not(.default)").remove().trigger('change');
            $.each(brands[$(this).val()]['models'], function (key, value) {
                var newOption = $('<option value="' + key +'">' + value + '</option>');

                $("#models").append(newOption).trigger('change');
            });
        });
    });
</script>