<section class="reviews">
    <div class="reviews_container">
        <h2 class="reviews_title">
            Отзывы наших клиентов
        </h2>
        <ul class="reviews_list">
            @for($i=0;$i<3;$i++)
                <li class="reviews_item">
                    <div class="reviews_item-image" style="background-image: url('/img/review-photo.jpg')"></div>
                    <div class="reviews_item-content">
                        <h4 class="reviews_item-title">
                            Триумф авто премиум класс
                        </h4>
                        <p class="reviews_item-subtitle">
                            отлчичная работа, все в лучшем виде и качестве
                        </p>
                        <div class="reviews_item-stars">
                            <svg class="reviews_item-star">
                                <use xlink:href="#star"></use>
                            </svg>
                            <svg class="reviews_item-star">
                                <use xlink:href="#star"></use>
                            </svg>
                            <svg class="reviews_item-star">
                                <use xlink:href="#star"></use>
                            </svg>
                        </div>
                    </div>
                </li>
            @endfor
        </ul>
    </div>
</section>