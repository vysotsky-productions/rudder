<section class="contacts">
    <div class="contacts_container">
        <div class="contacts_box mod_left">
            <svg class="contacts_logo">
                <use xlink:href="#logo"></use>
            </svg>
            <h2 class="contacts_title">Мы заставляем сервисы бороться
                за ваш автомобиль</h2>
        </div>
        <div class="contacts_box">
            <ul class="contacts_social">
                <li class="contacts_social-item">
                    <a class="contacts_social-link" href="">
                        <svg class="contacts_social-icon"><use xlink:href="#vk"></use></svg>
                    </a>
                </li>
                <li class="contacts_social-item">
                    <a class="contacts_social-link" href="">
                        <svg class="contacts_social-icon"><use xlink:href="#facebook"></use></svg>
                    </a>
                </li>
                <li class="contacts_social-item">
                    <a class="contacts_social-link" href="">
                        <svg class="contacts_social-icon"><use xlink:href="#rss"></use></svg>
                    </a>
                </li>
                <li class="contacts_social-item">
                    <a class="contacts_social-link" href="">
                        <svg class="contacts_social-icon"><use xlink:href="#mail"></use></svg>
                    </a>
                </li>
            </ul>

            <ul class="contacts_links">
                <li class="contacts_item">
                    <a class="contacts_item-link" href="">Личный кабинет</a>
                </li>
                <li class="contacts_item">
                    <a class="contacts_item-link" href="">Для автосервисов</a>
                </li>
                <li class="contacts_item">
                    <a class="contacts_item-link" href="">Партнерство</a>
                </li>
                <li class="contacts_item">
                    <a class="contacts_item-link" href="">Политика конфидениальности</a>
                </li>
            </ul>
        </div>
    </div>
</section>