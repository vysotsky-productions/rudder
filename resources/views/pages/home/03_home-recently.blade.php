<section class="recently">
    <div class="recently_container">
        <div class="recently_box mod_left">
            <h2 class="recently_title">Недавно нашли на портале</h2>
        </div>
        <div class="recently_box mod_right">
            <div class="recently_box-content">
                <div class="recently_swiper swiper-container">
                    <ul class="recently_list swiper-wrapper">
                        @for($i=0;$i<10;$i++)
                            <li class="recently_item swiper-slide">
                                <span>крышка багажника</span>
                                <span>19 000 рублей</span>
                                <span>honda civic 2011</span>
                            </li>
                        @endfor
                    </ul>
                </div>
                <button class="recently_swiper-button mod_prev">
                    <
                </button>
                <button class="recently_swiper-button mod_next">
                    >
                </button>
            </div>
        </div>
    </div>
</section>