<!DOCTYPE html>
<html>
<head>
    <title>Rudder</title>
    {{--<meta name="description" content="{{ $meta_description }}">--}}
    {{--<meta name="keywords" content="{{ $meta_keywords }}">--}}
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <meta http-equiv="cleartype" content="on">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script>window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token()]) !!};</script>
    <link rel="stylesheet" href="/fonts.css" type="text/css"/>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}" type="text/css"/>
</head>
<body class='page_{{ Route::currentRouteName() }}' data-page="{{ Route::currentRouteName() }}">
<div class="container">
    @include('sprite')
    @include('components.header')
    <main class="main">
        @yield('content')
    </main>
    @include('components.footer')
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>