<header>
    <div class="header">
        <div class="header_container">
            <div class="header_box">
                <a class="header_logo" href="/">
                    <svg class="header_logo-icon">
                        <use xlink:href="#logo"></use>
                    </svg>
                </a>
                <nav class="header_menu">
                    <div class="header_menu-container">
                        <ul class="header_menu-list">
                            <li class="header_menu-item">
                                <a class="header_menu-link" href="">
                                    Подбор автосервиса
                                </a>
                            </li>
                            <li class="header_menu-item">
                                <a class="header_menu-link" href="">
                                    Поиск автозапчастей
                                </a>
                            </li>
                            <li class="header_menu-item">
                                <a class="header_menu-link" href="">
                                    Автострахование
                                </a>
                            </li>
                            <li class="header_menu-item">
                                <a class="header_menu-link" href="">
                                    Автокредитование
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="header_location">
                <svg class="header_location-pin">
                    <use xlink:href="#pin"></use>
                </svg>
                <span class="header_location-title">Екатеринбург</span>
            </div>

            <div class="hamburger">
                <div class="hamburger_container">
                    <span class="hamburger_line"></span>
                </div>
            </div>
        </div>
    </div>
</header>