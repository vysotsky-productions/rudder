<style>
    @font-face
    {
        font-family: PTSansRegular;
        src:         local("PT Sans"), local("PTSansRegular"), url(fonts/PTSans.Regular.woff) format("woff"), url(fonts/PTSans.Regular.ttf) format("truetype"), url(fonts/PTSans.Regular.svg#PTSans-Regular) format("svg")
    }

    @font-face
    {
        font-family: PTSansBold;
        src:         local("PT Sans Bold"), local("PTSansBold"), url(fonts/PTSans.Bold.woff) format("woff"), url(fonts/PTSans.Bold.ttf) format("truetype"), url(fonts/PTSans.Bold.svg#PTSans-Bold) format("svg")
    }

    @font-face
    {
        font-family: PTSansItalic;
        src:         local("PT Sans Italic"), local("PTSansItalic"), url(fonts/PTSans.Italic.woff) format("woff"), url(fonts/PTSans.Italic.ttf) format("truetype"), url(fonts/PTSans.Italic.svg#PTSans-Italic) format("svg")
    }

    @font-face
    {
        font-family: PTSansBoldItalic;
        src:         local("PT Sans Bold Italic"), local("PTSansBoldItalic"), url(fonts/PTSans.Bold.Italic.woff) format("woff"), url(fonts/PTSans.Bold.Italic.ttf) format("truetype"), url(fonts/PTSans.Bold.Italic.svg#PTSans-BoldItalic) format("svg")
    }

    /**/
    .clear
    {
        clear: both
    }

    nav .nav-list
    {
        list-style: none;
        width:      100%;
        padding:    0;
        margin:     0
    }

    nav .nav-list li
    {
        display:        inline-block;
        text-align:     center;
        vertical-align: middle;
        position:       relative
    }

    .b-head-count nav .nav-list li a
    {
        display:     block;
        padding:     0 17px;
        height:      48px;
        line-height: 48px;
        font-size:   15px;
        font-family: PTSansBold
    }

    .b-head-count nav .nav-list li span
    {
        display:          block;
        padding:          0 17px;
        height:           48px;
        line-height:      48px;
        font-size:        15px;
        font-family:      PTSansBold;
        background-color: #ccdded
    }

    .b-footer-count
    {
        margin: 5px 0 25px
    }

    .b-footer-count nav .nav-list
    {
        display: table
    }

    .b-footer-count nav .nav-list li
    {
        display: table-cell
    }

    /*//*/

    @font-face
    {
        font-family: "PT_Sans-Bold";
        src:         url("../css/fonts/PTSans.Bold.ttf");
        font-weight: 400;
        font-style:  normal
    }

    .footer-social
    {
        min-width: 200px
    }

    .footer-social p
    {
        margin: 14px 0 7px
    }

    .l-footer
    {
        position:   relative;
        overflow:   hidden;
        height:     100px;
        background: url(../img/l-footer.jpg)
    }

    .b-footer
    {
        box-sizing: border-box;
        padding:    29px 5px 0
    }

    .b-footer__block
    {
        display:        inline-block;
        vertical-align: top
    }

    .b-footer__logo
    {
        margin: 3px 0 0
    }

    .b-footer__phone
    {
        margin: 3px 0 0 22px;
        font:   40px/1 PTSansBold;
        color:  #434343
    }

    .b-footer__phone p
    {
        margin: 0
    }

    .b-footer__address
    {
        margin:      0 0 0 12px;
        font-size:   15px;
        line-height: 18px;
        color:       #434343;
        text-align:  center
    }

    .b-footer__address p
    {
        margin: 0
    }

    .b-footer__sa
    {
        float:      right;
        text-align: right
    }

    .b-footer__sa-image
    {
        display: block
    }

    .b-footer__sa-text
    {
        display:        inline-block;
        margin:         3px 0 0;
        vertical-align: top;
        font-size:      10px;
        line-height:    1.1;
        color:          #4cb1d7
    }

    .b-footer__sa-text:hover
    {
        padding: 0 0 1px;
        border:  none
    }

    .ui-tabs .ui-tabs-nav
    {
        margin:  0;
        padding: .2em .2em 0
    }

    .ui-tabs .ui-tabs-nav li
    {
        list-style:          none;
        float:               left;
        position:            relative;
        top:                 0;
        margin:              1px .2em 0 0;
        border-bottom-width: 0;
        padding:             0;
        white-space:         nowrap
    }

    .ui-tabs .ui-tabs-nav .ui-tabs-anchor
    {
        float:           left;
        padding:         .5em 1em;
        text-decoration: none
    }

    .ui-tabs .ui-tabs-nav li.ui-tabs-active
    {
        margin-bottom:  -1px;
        padding-bottom: 1px
    }

    .ui-tabs .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor, .ui-tabs .ui-tabs-nav li.ui-state-disabled .ui-tabs-anchor, .ui-tabs .ui-tabs-nav li.ui-tabs-loading .ui-tabs-anchor
    {
        cursor: text
    }

    .ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor
    {
        cursor: pointer
    }

    .ui-tabs .ui-tabs-panel
    {
        display:      block;
        border-width: 0;
        padding:      1em 1.4em;
        background:   none
    }

    footer
    {
        font:  14px/1.3 PTSansRegular, Arial, Helvetica, sans-serif;
        color: #000
    }

    footer a
    {
        text-decoration: underline;
    }

    .spare-l-footer
    {
        height:         initial;
        color:          #252525;
        font-size:      14px;
        padding-top:    20px;
        padding-bottom: 20px;
        line-height:    17px
    }

    .spare-l-footer a
    {
        color: #252525
    }

    .footer-copyright
    {
        margin-top: 15px
    }

    .footer-left, .footer-right
    {
        max-width: 480px;
        width:     100%;
        margin:    auto;
    }

    .footer-right-bottom
    {
        display:    inline-block;
        text-align: right;
        max-width:  445px;
        width:      100%;
    }

    .footer-menu, .footer-social-list
    {
        list-style: none;
        margin:     0;
        padding:    0
    }

    .footer-menu-right
    {
        float: right
    }

    .footer-menu-left
    {
        float:        left;
        margin-right: 60px
    }

    .footer-menu li
    {
        margin-right: 15px
    }

    .footer-menu-right
    {
        padding-bottom: 63px
    }

    .footer-menu-url
    {
        color: #252525
    }

    .footer-social-list li
    {
        display:      inline-block;
        margin-right: 5px
    }

    .footer-social-icon
    {
        height:           20px;
        width:            20px;
        background-color: #fff;
        display:          block
    }

    .footer-contacts, .footer-social, .footer-developer
    {
        display: inline-block
    }

    .footer-contacts
    {
        max-width: 330px;
        width:     100%;
    }

    .developer-url
    {
        display: block
    }

    span + b.footer-address
    {
        padding-top: 10px;
        display:     inline-block
    }

    .footer-developer
    {
        text-align:  right;
        margin-left: 25px
    }

    .developer-logo
    {
        background-image: url("../img/footer-logo2.png");
        width:            90px;
        height:           22px;
        display:          inline-block
    }

    .fsc-vk
    {
        background-image: url("../img/footer-vk.jpg")
    }

    .fsc-fb
    {
        background-image: url("../img/footer-fb.jpg")
    }

    .fsc-tw
    {
        background-image: url("../img/footer-tw.jpg")
    }

    .fsc-mail
    {
        background-image: url("../img/footer-mail.jpg")
    }

    .fsc-ok
    {
        background-image: url("../img/footer-ok.jpg")
    }

    .fsc-inst
    {
        background-image: url("../img/footer-inst.jpg")
    }

    .fsc-gp
    {
        background-image: url("../img/footer-gp.jpg")
    }

    .b-wrapper
    {
        max-width: 1180px;
        width:     100%;
        margin:    0 auto
    }

    .ui-helper-clearfix:before, .ui-helper-clearfix:after
    {
        content:         "";
        display:         table;
        border-collapse: collapse
    }

    .ui-helper-clearfix:after
    {
        clear: both
    }

    .ui-helper-clearfix
    {
        min-height: 0
    }

    .i-clearfix:after, .i-clearfix:before
    {
        content: " ";
        display: table
    }

    .i-clearfix:after
    {
        clear: both
    }

    a:hover
    {
        text-decoration: none
    }
</style>

<div class="l-footer spare-l-footer">
    <footer class="b-wrapper">
        <div class="b-footer-count">
            <nav>
                <ul class="nav-list">
                    <li>
                        <a class="nav-item" href="/car_repair">Оценка ремонта</a>
                    </li>
                    <li>
                        <a class="nav-item" href="/car_parts">Поиск автозапчастей</a>
                    </li>
                    <li>
                        <a class="nav-item" href="/avtoservisy">Автосервисы</a>
                    </li>
                    <li>
                        <a class="nav-item" href="/avtomagaziny">Автомагазины</a>
                    </li>
                    <li>
                        <a class="nav-item" href="/avtostrakhovanie">Автострахование</a>
                    </li>
                    <li>
                        <a class="nav-item" href="/avtokredit">Автокредитование</a>
                    </li>
                    <li>
                        <a class="nav-item" href="/poleznye-sovety">Полезные советы</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="footer-left">
            <div class="footer-contacts">
                <b class="footer-address">Служба поддержки интернет-портала:</b>
                <span class="footer-address">г. Екатеринбург, Сибирский тракт, 12, строение 8, оф. 320</span>

                <span class="footer-address"><span class="callibri_phone">+7 (343) 207-22-20</span>,
                    <a href="mailto:support@sp66.pro">support@sp66.pro</a>
                </span>
                <span class="footer-address">Режим работы: ПН-ПТ 9:30 - 18:00, СБ-ВС выходной</span>
                <!--                    --><!--,-->
                <!--                    <a href="mailto:--><!--">--><!--</a>-->
                <b class="footer-address">Рекламный отдел:</b>
                <span class="footer-address"><span class="callibri_phone">+7 (343) 207-22-20</span>,
                    <a href="mailto:reklama@sp66.pro')">reklama@sp66.pro</a>
                </span>
                <span class="footer-address">Режим работы: ПН-ПТ 9:30 - 18:00, СБ-ВС выходной</span>
                <b class="footer-address">Отзывы и предложения о работе портала:</b>
                <span class="footer-address"><span class="callibri_phone">+7 (343) 207-22-20</span>,
                    <a href="mailto:info@sp66.pro">info@sp66.pro</a>
                </span>
                <span class="footer-address">Режим работы: ПН-ПТ 9:30 - 18:00, СБ-ВС выходной</span>
            </div>
            <div class="footer-copyright">
                © Сервис-Про, интернет-портал
                по поиску автозапчастей и автосервисов
            </div>
        </div>
        <div class="footer-right">
            <div class="footer-right-top">
                <div class="footer-menu-left">
                    <ul class="footer-menu">
                        <li><a class="footer-menu-url" href="/content/advertisement">Реклама на сайте</a></li>
                        <li><a class="footer-menu-url" href="/content/partnership">Партнерство</a></li>
                        <li><a class="footer-menu-url" href="/content/autoservice">Автосервисам</a></li>
                        <li><a class="footer-menu-url" href="/content/shops">Магазинам</a></li>
                        <li><a class="footer-menu-url" href="/content/insurance_company">Страховым компаниям</a></li>
                    </ul>
                </div>
                <div class="footermenu-ridht">
                    <ul class="footer-menu">
                        <li><a class="footer-menu-url" href="/prices">Тарифы</a></li>
                        <li><a class="footer-menu-url" href="/files/userfiles/SP66_user_pact.pdf">Правила пользования
                                сайтом</a></li>
                        <li><a class="footer-menu-url" href="/requests">Лента заявок</a></li>
                        <li><a class="footer-menu-url" href="/insure">Страхование</a></li>
                        <li><a class="footer-menu-url" href="/kredit">Автокредитование</a></li>
                    </ul>
                    <div class="footer-social">
                        <p>Ищите нас в</p>
                        <ul class="footer-social-list">
                            <li><a href="https://vk.com/sp66_ekb" class="footer-social-icon fsc-vk"></a></li>
                            <li><a href="https://www.facebook.com/groups/spro66/" class="footer-social-icon fsc-fb"></a>
                            </li>
                            <!--                        <li><a href="#" class="footer-social-icon fsc-tw"></a></li>-->
                            <!--                        <li><a href="#" class="footer-social-icon fsc-mail"></a></li>-->
                            <li><a href="https://ok.ru/group/54672872439809" class="footer-social-icon fsc-ok"></a></li>
                            <!--                        <li><a href="#" class="footer-social-icon fsc-inst"></a></li>-->
                            <!--                        <li><a href="#" class="footer-social-icon fsc-gp"></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-right-bottom">

                <div class="footer-developer">
                    <a href="#" class="developer-url developer-logo"></a>
                    <a href="http://siteactiv.ru/" class="developer-url">Разработка портала</a>
                    <a href="http://promo-sa.ru/" class="developer-url">Продвижение портала</a>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <!-- Yandex.Metrika counter -->
        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script type="text/javascript" async="" src="https://mc.yandex.ru/metrika/watch.js"></script>
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter30965231 = new Ya.Metrika({
                            id: 30965231,
                            webvisor: true,
                            clickmap: true,
                            trackLinks: true,
                            accurateTrackBounce: true
                        });
                    } catch (e) {
                    }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () {
                        n.parentNode.insertBefore(s, n);
                    };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript>&lt;div&gt;&lt;img src="//mc.yandex.ru/watch/30965231" style="position:absolute; left:-9999px;" alt=""
            /&gt;&lt;/div&gt;
        </noscript>
        <!-- /Yandex.Metrika counter -->

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-64248372-1', 'auto');
            ga('send', 'pageview');

        </script>

        <!--LiveInternet counter-->
        <script type="text/javascript"><!--
            document.write("<a href='//www.liveinternet.ru/click' " +
                "target=_blank><img src='//counter.yadro.ru/hit?t26.10;r" +
                escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
                    ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                    screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                ";" + Math.random() +
                "' alt='' title='LiveInternet: показано число посетителей за" +
                " сегодня' " +
                "border='0' width='1' height='1'><\/a>")
            //--></script>
        <a href="//www.liveinternet.ru/click" target="_blank"><img
                    src="//counter.yadro.ru/hit?t26.10;r;s1920*1080*24;uhttps%3A//sp66.pro/;0.3804077400813466" alt=""
                    title="LiveInternet: показано число посетителей за сегодня" border="0" width="1" height="1"></a>
        <!--/LiveInternet-->

    </footer>
</div>


