<div class="reg-drivers">
    <form class="reg-drivers_form" action="">
        <h4 class="reg-drivers_title">Бесплатно оставьте свои данные</h4>
        <input class="reg-drivers_input" type="text" name="email" id="email" placeholder="Контактный телефон">
        <button class="reg-drivers_enter" type="submit">Получить доступ в личный кабинет</button>

        <p class="reg-drivers_policy">Нажимая эту кнопку, Вы подтверждаете, что ознакомлены и согласны с политикой обработки персональных данных.</p>
    </form>
</div>