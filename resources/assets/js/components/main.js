$(window).on('load', function () {
    var recentlySwiper = new Swiper('.recently_swiper', {
        slidesPerView: 'auto',
        initialSlide: 3,
        centeredSlides: true,
        loop: false,
        centerMode: true,
        spaceBetween: 45,
        speed: 300,
        grabCursor: true,
        nextButton: '.recently_swiper-button.mod_next',
        prevButton: '.recently_swiper-button.mod_prev'
    });


    $('.hamburger').on('click', function () {
        if (!$(this).is('.mod_active')) {
            $(this).addClass('mod_active');
            $('.header_menu').addClass('mod_active');
            $('body').addClass('noscroll');
        } else {
            $(this).removeClass('mod_active');
            $('.header_menu').removeClass('mod_active');
            $('body').removeClass('noscroll');
        }
    });
});

