$(window).on('load', function () {
    $('#play-button').click(function () {
        var video_src = $(this).attr('data-src');
        $(this).remove();

        $('.video_box.mod_right .video_box-container').prepend('<iframe class="video_iframe"></iframe>');

        $('.video_iframe').attr('src', video_src + '?autoplay=1');
        return false;

    });
});