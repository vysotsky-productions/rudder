$('.era_button').click(function () {
    $('.auth').slideToggle();

    return false;
});


$('.auth_reg').click(function () {
    $('.reg').slideDown();

    $('.auth, .era_button').slideUp();
    return false;
});

// Tabs
$('.reg_link.mod_1').click(function () {
    $('.reg_tab.mod_1, .reg_link.mod_1').addClass('mod_active').siblings().removeClass('mod_active');
    $('.reg').removeClass('mod_300');
    return false;
});

$('.reg_link.mod_2').click(function () {
    $('.reg_tab.mod_2, .reg_link.mod_2').addClass('mod_active').siblings().removeClass('mod_active');
    $('.reg').addClass('mod_300');
    return false;
});